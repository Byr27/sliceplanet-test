class FormFields {
  constructor({
    email = '',
    password = '',
    checkPassword = '',
    isValideEmail = false,
    isValidePassword = false,
    isValideCheckPassword = false,
  } = {}) {
    Object.assign(this, {
      email,
      password,
      checkPassword,
      isValideEmail,
      isValidePassword,
      isValideCheckPassword,
    });
  }
}

export default function (data) {
  return new FormFields(data);
}
