export default {
  getProducts({ commit }) {
    commit('updateState', {
      products: [
        {
          id: 1,
          name: 'Ноутбук Mi Notebook Pro 2 15.6',
          price: 23999,
          image: 'src/images/xiaomi.jpg',
        },
        {
          id: 2,
          name: 'Ноутбук Xiaomi Mi Gaming Notebook 15,6',
          price: 40000,
          image: 'src/images/xiaomi-gaming.jpg',
        },
        {
          id: 3,
          name: 'Apple MacBook Pro Touch Bar 15',
          price: 77000,
          image: 'src/images/macbook.jpg',
        },
        {
          id: 4,
          name: 'Apple iMac 27',
          price: 54999,
          image: 'src/images/imac.jpg',
        },
      ],
    });
  },

  getLoggedInUser({ commit }) {
    const loggedInUser = localStorage.getItem('loggedInUser');

    commit('updateState', {
      loggedInUser: loggedInUser
        ? JSON.parse(loggedInUser)
        : {},
    });
  },

  addUser({ commit }, user) {
    const userId = new Date().valueOf();

    const loggedInUser = {
      ...user,
      id: userId,
      isLoggedUser: true,
      basket: [],
    };

    localStorage.setItem('loggedInUser', JSON.stringify(loggedInUser));
    commit('updateState', { loggedInUser });
  },

  logOut({ commit }) {
    const loggedInUser = {};

    localStorage.setItem('loggedInUser', JSON.stringify(loggedInUser));
    commit('updateState', { loggedInUser });
  },

  changeBusket({ commit, state }, operationInfo) {
    const { loggedInUser } = state;
    const {
      productId,
      productAmount,
    } = operationInfo;

    loggedInUser.basket.forEach((product) => {
      if (product.id === productId) {
        product.amount += productAmount;
      }
    });

    const reduce = loggedInUser.basket.reduce((equal, item) => {
      let isEqual = equal;
      if (item.id === productId) {
        isEqual += 1;
      }
      return isEqual;
    }, 0);

    if (reduce === 0) {
      loggedInUser.basket.push(
        {
          id: productId,
          amount: productAmount,
          time: new Date().valueOf(),
        },
      );
    }

    loggedInUser.basket.sort((a, b) => +b.time - +a.time);

    localStorage.setItem('loggedInUser', JSON.stringify(loggedInUser));
    commit('updateState', { loggedInUser });
  },

  removeBasketItem({ commit, state }, id) {
    const { loggedInUser } = state;

    loggedInUser.basket = loggedInUser.basket.filter(basketItem => basketItem.id !== id);

    localStorage.setItem('loggedInUser', JSON.stringify(loggedInUser));
    commit('updateState', { loggedInUser });
  },

  clearBasket({ commit, state }) {
    const { loggedInUser } = state;

    loggedInUser.basket = [];

    localStorage.setItem('loggedInUser', JSON.stringify(loggedInUser));
    commit('updateState', { loggedInUser });
  },
};
