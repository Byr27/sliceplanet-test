export default {
  isLoggedUser: state => state.loggedInUser.isLoggedUser,
  loggedInUserEmail: state => state.loggedInUser.email,
  basket: state => state.loggedInUser.basket,
  basketItemsAmount: state => state.loggedInUser.isLoggedUser
    && state.loggedInUser.basket.reduce((acc, item) => acc + item.amount, 0),

  totalCoast: state => state.loggedInUser.isLoggedUser
    && state.loggedInUser.basket.reduce((acc, item) => {
      let coast = acc;

      state.products.forEach((product) => {
        if (product.id === item.id) {
          coast += product.price * item.amount;
        }
      });

      return coast;
    }, 0),
};
