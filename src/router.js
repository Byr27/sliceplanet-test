import Vue from 'vue';
import VueRouter from 'vue-router';

import TheHome from '@pages/TheHome';
import TheBasket from '@pages/TheBasket';
import TheRegistration from '@pages/TheRegistration';

Vue.use(VueRouter);


export default new VueRouter({
  routes: [
    {
      path: '/',
      component: TheHome,
    },
    {
      path: '/basket',
      component: TheBasket,
    },
    {
      path: '/registration',
      component: TheRegistration,
    },
  ],
});
